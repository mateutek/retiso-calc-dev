'use strict';

angular.module('retisoApp')
  .service('ConfiguratorService', function ($http) {
    var globalConfig = null;

    var promise = $http.get('/config.json').success(function (config) {
      globalConfig = config;
    });

    return {
      promise:promise,
      setData: function (config) {
        globalConfig = config;
      },
      doStuff: function () {
        return globalConfig;
      }
    };
  });
