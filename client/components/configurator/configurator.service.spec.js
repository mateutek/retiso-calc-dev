'use strict';

describe('Service: configurator', function () {

  // load the service's module
  beforeEach(module('retisoApp'));

  // instantiate service
  var configurator;
  beforeEach(inject(function (_configurator_) {
    configurator = _configurator_;
  }));

  it('should do something', function () {
    expect(!!configurator).toBe(true);
  });

});
