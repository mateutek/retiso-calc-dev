'use strict';

angular.module('retisoApp')
  .directive('levels', function ($interpolate) {
    return {
      templateUrl: 'components/levels/levels.html',
      restrict: 'EA',
      transclude: true,
      link: function ($scope) {
        $scope.levelsPieLabels = ['Obecna','Następny poziom'];
        $scope.levelsPieData =[];
        $scope.levels = [];
        $scope.$watchCollection('model',function(){
          $scope.levels = [];
          $scope.levelsPieData =[];
          var tmpPpl = 0;
          var tmp = {};
          var active = '';
          $scope.bonusLevels = 0;
          $scope.levelsTotal = 0;

          for(var i=1;i<=10;i++){
            active=i<=$scope.model.levels?'retiso-yellow':'active';
            if(i===1){
              tmpPpl = $scope.model.tip;
            }else if(i===2){
              tmpPpl = tmpPpl*tmpPpl;
            }
            else{
              tmpPpl = tmpPpl*$scope.model.tip;
            }
            tmp={
              ppl:tmpPpl,
              basket:tmpPpl*$scope.model.total,
              bonus:tmpPpl*$scope.model.total*($scope.model.bonus/100),
              class:active
            };
            $scope.levelsTotal=$scope.levelsTotal+tmp.bonus;

            $scope.bonusLevels=i<=$scope.model.levels?$scope.bonusLevels+tmp.bonus:$scope.bonusLevels;

            $scope.levels.push(tmp);
          }
          $scope.levelsPieData.push($scope.bonusLevels);
          $scope.levelsPieData.push($scope.levelsTotal-$scope.bonusLevels);

          $scope.potential = ($scope.bonusLevels/$scope.levelsTotal)*100;

          $scope.levelText = $interpolate($scope.levelsTexts[$scope.model.levels])($scope);
          $scope.level2Text = $interpolate($scope.levels2Texts[$scope.model.levels])($scope);

        });
      }
    };
  });
