'use strict';

describe('Directive: levels', function () {

  // load the directive's module and view
  beforeEach(module('retisoApp'));
  beforeEach(module('components/levels/levels.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<levels></levels>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the levels directive');
  }));
});