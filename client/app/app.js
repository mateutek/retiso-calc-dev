'use strict';

angular.module('retisoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngMessageFormat',
  'ui.router',
  'ui.bootstrap',
  'chart.js',
  'bm.bsTour'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);

  });
