'use strict';

angular.module('retisoApp')
  .controller('MainCtrl', function ($scope,ConfiguratorService) {
    var mainConfig=ConfiguratorService.doStuff();

    $scope.date = new Date();

    $scope.tourEnabled = mainConfig.tourEnabled;

    $scope.informationsRow = true;
    $scope.instructionsRow = true;

    $scope.pieOptions = mainConfig.pieOptions;
    $scope.levelsPieOptions={
      showTooltips:false
    };

    $scope.pieColors = ['#ed5565','#46BFBD','#FDBB04','#97BBCD'];

    $scope.levelsPieColors = ['#FDBB04','#F5F5F5'];

    $scope.retisoConstant = mainConfig.retisoConstant;

    $scope.automatic = mainConfig.automatic;

    $scope.PRqualification = mainConfig.PRqualification;

    $scope.model=mainConfig.model;

    $scope.defaults = mainConfig.defaults;

    $scope.defaultGroceryOptions = mainConfig.defaultGroceryOptions;

    $scope.defaultFuelOptions = mainConfig.defaultFuelOptions;

    $scope.defaultClothesOptions = mainConfig.defaultClothesOptions;

    $scope.defaultOtherOptions = mainConfig.defaultOtherOptions;

    $scope.defaultRetisoOptions = mainConfig.defaultRetisoOptions;

    $scope.defaultTipOptions = mainConfig.defaultTipOptions;

    $scope.defaultBonusOptions = mainConfig.defaultBonusOptions;

    $scope.defaultLevelsOptions = mainConfig.defaultLevelsOptions;

    $scope.groceryOptions=angular.extend({},$scope.defaults,$scope.defaultGroceryOptions);
    $scope.fuelOptions=angular.extend({},$scope.defaults,$scope.defaultFuelOptions);
    $scope.clothesOptions=angular.extend({},$scope.defaults,$scope.defaultClothesOptions);
    $scope.otherOptions=angular.extend({},$scope.defaults,$scope.defaultOtherOptions);
    $scope.retisoOptions=angular.extend({},$scope.defaults,$scope.defaultRetisoOptions);
    $scope.tipOptions=angular.extend({},$scope.defaults,$scope.defaultTipOptions);
    $scope.bonusOptions=angular.extend({},$scope.defaults,$scope.defaultBonusOptions);
    $scope.levelsOptions=angular.extend({},$scope.defaults,$scope.defaultLevelsOptions);

    $scope.defaultTotalOptions = {
      min:0,
      max:($scope.groceryOptions.max+$scope.fuelOptions.max+$scope.clothesOptions.max+$scope.otherOptions.max),
      postfix: ' PLN',
      step:1
    };

    $scope.totalOptions=angular.extend({},$scope.defaults,$scope.defaultTotalOptions);

    $scope.labels = ['art. spoż.', 'paliwo', 'art.chem., odzież', 'inne'];
    $scope.data = [$scope.model.grocery, $scope.model.fuel,$scope.model.clothes, $scope.model.other];
    $scope.tmpTotal = (parseInt($scope.model.grocery,10) + parseInt($scope.model.fuel,10) + parseInt($scope.model.clothes,10) + parseInt($scope.model.other,10))*parseFloat($scope.model.retiso/100);

    $scope.$watchGroup(['model.grocery','model.fuel','model.clothes','model.other','model.retiso'],function() {
      $scope.defaultTotalOptions = {
        min:0,
        max:($scope.groceryOptions.max+$scope.fuelOptions.max+$scope.clothesOptions.max+$scope.otherOptions.max),
        postfix: ' PLN',
        step:1
      };
      $scope.totalOptions=angular.extend({},$scope.defaults,$scope.defaultTotalOptions);

      $scope.tmpTotal = (parseInt($scope.model.grocery,10) + parseInt($scope.model.fuel,10) + parseInt($scope.model.clothes,10) + parseInt($scope.model.other,10))*parseFloat($scope.model.retiso/100);
      $scope.model.total = parseInt($scope.tmpTotal);
      $scope.data = [$scope.model.grocery, $scope.model.fuel, $scope.model.clothes, $scope.model.other];
      $scope.yearBonus = $scope.model.total*12*$scope.retisoConstant;
      if($scope.automatic){
        var tmpModelLevels = Math.floor($scope.model.total/$scope.PRqualification);
        $scope.model.levels = tmpModelLevels <=10?tmpModelLevels:10;
      }
    });

    $scope.$watch('model.total',function(){
      $scope.yearBonus = $scope.model.total*12*$scope.retisoConstant;
      if($scope.automatic){
        var tmpModelLevels = Math.floor($scope.model.total/$scope.PRqualification);
        $scope.model.levels = tmpModelLevels <=9?tmpModelLevels:9;
      }
    });

    $scope.levelsTexts=mainConfig.levelsTexts;
    $scope.levels2Texts=mainConfig.levels2Texts;

    $scope.tourTemplate='<div class="popover tour">'+
    '<div class="arrow"></div>'+
    '<h3 class="popover-title"></h3>'+
    '<div class="popover-content"></div>'+
    '<div class="popover-navigation">'+
      '<div class="btn-group">'+
        '<button class="btn btn-info" data-role="prev">« Pop</button>'+
        '<button class="btn btn-info" data-role="next">Nast »</button>'+
        '<button class="btn btn-danger" data-role="end">Zakończ</button>'+
      '</div>'+
    '</div>'+
    '</nav>'+
    '</div>';


    $scope.hideRow = function(element){
      $scope[element]=!$scope[element];
    };


  });
