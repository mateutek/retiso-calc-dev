'use strict';

angular.module('retisoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl',
        resolve:{
          'ConfiguratorServiceData':function(ConfiguratorService){
            // ConfiguratorServiceData will also be injectable in your controller, if you don't want this you could create a new promise with the $q service
            return ConfiguratorService.promise;
          }
        }
      });
  });
